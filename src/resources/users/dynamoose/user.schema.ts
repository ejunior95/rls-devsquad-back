import { Schema } from 'dynamoose';

export const UserSchema = new Schema({
  id: {
    type: String,
    hashKey: true,
  },
  email: {
    type: String,
  },
  name: {
    type: String,
  },
  isAdmin: {
    type: Boolean,
  },
});