import { Field, ID, ObjectType } from "@nestjs/graphql";
import { IUser } from "../../dynamoose/user.interface";

@ObjectType()
export class User implements IUser {
  @Field(() => ID, { nullable: true })
  id: string;
  
  @Field(() => String, { nullable: true })
  email: string;
  
  @Field(() => String, { nullable: true })
  name: string;
  
  @Field(() => Boolean, { nullable: true })
  isAdmin: boolean;

}