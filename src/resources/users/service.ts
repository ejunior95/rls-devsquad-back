import { Injectable } from '@nestjs/common';
import { InjectModel, Model } from 'nestjs-dynamoose';
import { IUser, IUserKey } from './dynamoose/user.interface';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User')
    private userModel: Model<IUser, IUserKey>,
  ) {}

  create(user: IUser) {
    return this.userModel.create(user);
  }

  update(key: IUserKey, user: Partial<IUser>) {
    return this.userModel.update(key, user);
  }

  findOne(key: IUserKey) {
    return this.userModel.get(key);
  }

  findAll() {
    return this.userModel.scan().exec();
  }
}