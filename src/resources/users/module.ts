import { Module, Global } from '@nestjs/common';
import { DynamooseModule } from 'nestjs-dynamoose';
import { UserSchema } from './dynamoose/user.schema';
import { UserResolver } from './resolver';

@Global()
@Module({
  imports: [
    DynamooseModule.forFeature([{
      name: 'User',
      schema: UserSchema,
      options: {
        tableName: 'Users',
      },
    }]),
  ],
  providers: [UserResolver],
  exports: [],
})
export class UserModule {
    
}
