import { Resolver, Query, Mutation, ResolveField, Parent, Args } from '@nestjs/graphql';
import { CreateOneArticleArgs } from './dto/args/create.args';
import { FindManyArticleArgs } from './dto/args/findMany.args';
import { Article, Articles } from './dto/model';
import { IArticle } from './dynamoose/article.interface';
import { ArticleService } from './service';

@Resolver(() => Article)
export class ArticleResolver {
  constructor(private readonly service: ArticleService) {}

  @Mutation(() => Article, { name: 'createArticle', nullable: true })
    create(@Args() args: CreateOneArticleArgs): Promise<Article> {
    return this.service.create(args)
  }
  
  @Query(() => Articles, { name: 'articles', nullable: true })
    findMany(@Args() args: FindManyArticleArgs) {
    return this.service.findMany(args)
  }
}