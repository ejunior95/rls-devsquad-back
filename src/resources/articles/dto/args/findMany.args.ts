import { ArgsType, Field, Int, } from "@nestjs/graphql";

@ArgsType()
export class FindManyArticleArgs {
    @Field(() => Int, { nullable: true })
    limit!: number;

    @Field(() => String, { nullable: true })
    startAtKey!: string;
}