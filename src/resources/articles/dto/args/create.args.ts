import { ArgsType, Field, InputType, PickType } from "@nestjs/graphql";
import { Article } from "../model";

@InputType()
export class ArticleCreateInput {

  @Field(() => String, { nullable: true })
  title: string;
  
  @Field(() => String, { nullable: true })
  content: string;
  
  @Field(() => String, { nullable: true })
  category: string;

  @Field(() => Boolean, { defaultValue: true })
  isDraft: boolean
}

@ArgsType()
export class CreateOneArticleArgs {
    @Field(() => ArticleCreateInput, {nullable:false})
    data!: ArticleCreateInput;
}