import { Field, ID, ObjectType } from "@nestjs/graphql";
import { Paginated } from "../../../../common/dto/pagination";
import { IArticle } from "../../dynamoose/article.interface";

@ObjectType()
export class Article implements IArticle {
  @Field(() => ID, { nullable: true })
  id: string;

  @Field(() => String, { nullable: true })
  title: string;
  
  @Field(() => String, { nullable: true })
  content: string;
  
  @Field(() => String, { nullable: true })
  category: string;
  
  @Field(() => String, { nullable: true })
  userId: string;
  
  @Field(() => Date, { nullable: true })
  createdAt: string;
  
  @Field(() => Date, { nullable: true })
  updatedAt: string;

  @Field(() => Boolean, { nullable: true })
  isDraft: boolean;
}

@ObjectType()
export class Articles extends Paginated(Article) {}