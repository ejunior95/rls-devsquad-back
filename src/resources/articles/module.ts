import { Module, Global } from '@nestjs/common';
import { DynamooseModule } from 'nestjs-dynamoose';
import { ArticleSchema } from './dynamoose/article.schema';
import { ArticleResolver } from './resolver';
import { ArticleService } from './service';

@Global()
@Module({
  imports: [
    DynamooseModule.forFeature([{
      name: 'Article',
      schema: ArticleSchema,
      options: {
        tableName: 'Articles',
      },
    }]),
  ],
  providers: [
    ArticleResolver,
    ArticleService
  ],
  exports: [],
})
export class ArticleModule {
    
}
