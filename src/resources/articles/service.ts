import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { InjectModel, Model } from 'nestjs-dynamoose';
import { CreateOneArticleArgs } from './dto/args/create.args';
import { IArticle, IArticleKey } from './dynamoose/article.interface';
import { FindManyArticleArgs } from './dto/args/findMany.args';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel('Article')
    private articleModel: Model<IArticle, IArticleKey>,
  ) {}

  create(article: CreateOneArticleArgs) {
    return this.articleModel.create({
      ...article.data,
      userId: uuidv4(),
      id: uuidv4(),
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString()
    });
  }

  update(key: IArticleKey, user: Partial<IArticle>) {
    return this.articleModel.update(key, user);
  }

  findOne(key: IArticleKey) {
    return this.articleModel.get(key);
  }

  async findMany(args: FindManyArticleArgs) {
    const startAtKey = args.startAtKey ? JSON.parse(args.startAtKey) : null
    const nodes = await this.articleModel.scan().limit(args.limit).startAt(startAtKey).exec();
    return {
      nodes,
      pageInfo: {
        lastKey: nodes.lastKey ? JSON.stringify(nodes.lastKey) : null 
      }
    }
  }
}