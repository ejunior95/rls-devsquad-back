import { Schema } from 'dynamoose';

export const ArticleSchema = new Schema({
  id: {
    type: String,
    hashKey: true,
  },
  title: {
    type: String,
  },
  content: {
    type: String,
  },
  category: {
    type: String,
  },
  userId: {
    type: String,
  },
  createdAt: {
    type: String,
  },
  updatedAt: {
    type: String,
  },
  isDraft: {
    type: Boolean,
  },
});