export interface IArticleKey {
    id: string;
  }
  
  export interface IArticle extends IArticleKey {
    title: string;
    content: string;
    category: string;
    userId: string;
    createdAt: string;
    updatedAt: string;
    isDraft: boolean;
  }