import type { Server } from 'http';
import type { APIGatewayProxyEvent, Context, Handler } from 'aws-lambda';

import express from 'express';
import serverless from 'aws-serverless-express';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ExpressAdapter } from '@nestjs/platform-express';
import { ValidationPipe } from '@nestjs/common';

export async function bootstrap() {
  const expressApp = express();
  const adapter = new ExpressAdapter(expressApp);
  const app = await NestFactory.create(AppModule, adapter);
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  await app.init();

  return serverless.createServer(expressApp);
}

let cachedServer: Server;
export async function handler(event: APIGatewayProxyEvent, context: Context): Handler {
  if (!cachedServer) {
    const server = await bootstrap();
    cachedServer = server;
  }
  return serverless.proxy(cachedServer, event, context, 'PROMISE').promise;
};