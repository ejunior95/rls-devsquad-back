import type { ApolloFederationDriverConfig } from '@nestjs/apollo';

import 'reflect-metadata';
import { Module } from '@nestjs/common';
import { DynamooseModule } from 'nestjs-dynamoose';
import { ApolloFederationDriver } from '@nestjs/apollo';
import { GraphQLModule } from '@nestjs/graphql';
import { CommonModule } from './common/common.module';
import { ArticleModule } from './resources/articles/module';
import { UserModule } from './resources/users/module';

@Module({
  imports: [
    DynamooseModule.forRoot(),
    GraphQLModule.forRootAsync<ApolloFederationDriverConfig>({
      driver: ApolloFederationDriver,

      useFactory: () => {
        const endpoint = 'graphql';
        const schemaModuleOptions: ApolloFederationDriverConfig = {
          autoSchemaFile: { federation: 2 },
          sortSchema: true,
          playground: { endpoint },
          introspection: true,
          path: endpoint,
        };
        return {
          context: ({ req }) => req,
          ...schemaModuleOptions,
        };
      },
    }),
    CommonModule,
    ArticleModule,
    UserModule
  ],
  providers: [],
  controllers: [],
})
export class AppModule {
  configure() {}
}
