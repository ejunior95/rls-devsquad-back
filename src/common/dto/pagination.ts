import { Type } from '@nestjs/common';
import { Field, ObjectType, Int } from '@nestjs/graphql';

export function Paginated<T extends Type>(classRef: T) {
  @ObjectType(`${classRef.name}Paginated`, { isAbstract: true })
  abstract class PageInfo {
    @Field(() => Boolean, {
      nullable: true,
    })
    hasNextPage?: number;

    @Field(() => String, { nullable: true })
    lastKey?: string;
  }

  @ObjectType({ isAbstract: true })
  abstract class Pagination {
    @Field(() => [classRef],)
    nodes: Array<T>;

    @Field(() => PageInfo, {
      nullable: true,
    })
    pageInfo?: PageInfo;
  }

  return Pagination;
}